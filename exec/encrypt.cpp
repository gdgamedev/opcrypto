/*
    Esse programa foi desenvolvido para uso como extens�o de outros programas,
    n�o foi desenvolvido para uso pessoal, ou seja, qualquer mensagem que vier aparecer no
    terminal de comando � destinado � leitura por outros programas
*/

#include <iostream>
#include <string>
#include <cstring>
#include <sstream>

#include <cstdlib>
#include <stdio.h>
#include <ctime>

#include <vector>

#include <fstream>
#include <iomanip>

#include <math.h>

#include <thread>
#include <mutex>

using namespace std;

bool print_progress = true;

double progress = 0.0;
mutex m;

vector <int> chars;

double getSetProgress(double p = -1.0) {
    lock_guard<mutex> lock(m);
    if (p == -1.0) {
        return progress;
    } else {
        progress = p;
        return 0.0;
    }
}

void stdoutprogress() {
    while (true) {
        bool _end = false;
        double cProgress = getSetProgress();
        cout << "{\"Progress\": ";
        if (cProgress > 0.0 && cProgress < 1.0) {
            cout << fixed << setprecision(2) << cProgress * 100.0;
        }
        if (cProgress >= 1.0) {
            cout << "100";
            _end = true;
        } else {
            cout << "0";
        }
        cout << "}" << endl;
        if (_end){
            break;
        }
    }
}

void generate_ascii_codes() {
    for (int i = 48; i <= 57; i++) {
        chars.push_back(i);
    }
    for (int i = 65; i <= 90; i++) {
        chars.push_back(i);
    }
    for (int i = 97; i <= 122; i++) {
        chars.push_back(i);
    }
}

template <typename T>
string NumberToString ( T Number ) {
    ostringstream ss;
    ss << Number;
    return ss.str();
}

bool stringHasEnding (string const &fullString, string const &ending) {
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));
    } else {
        return false;
    }
}

int Wrap(int kX, int const kLowerBound, int const kUpperBound)
{
    int range_size = kUpperBound - kLowerBound + 1;

    if (kX < kLowerBound)
        kX += range_size * ((kLowerBound - kX) / range_size + 1);

    return kLowerBound + (kX - kLowerBound) % range_size;
}

string encrypt(string text, string key) {
    string result = "";
    int text_size = text.size();
    int key_size = key.size();
    for (int text_i = 0; text_i < text_size; text_i++) {
        int result_char = (int)text.at(text_i);
        for (int key_i = 0; key_i < key_size; key_i++) {
            result_char += (int)key.at(key_i) * key_size;
        }
        result_char = Wrap(result_char, 0, 1114111);
        result += char(result_char);
    }
    return result;
}

string decrypt(string text, string key) {
    string result = "";
    int text_size = text.size();
    int key_size = key.size();
    for (int text_i = 0; text_i < text_size; text_i++) {
        int result_char = (int)text.at(text_i);
        if (result_char == 1114112) {
            result_char = 5385;
        }
        for (int key_i = 0; key_i < key_size; key_i++) {
            result_char -= (int)key.at(key_i) * key_size;
        }
        result_char = Wrap(result_char, 0, 1114111);
        result += char(result_char);
    }
    return result;
}

int randomNumberInt(int min_number, int max_number) {
    return rand()%(max_number + 1 - min_number);
}

void clear_file(string path) {
    ofstream file(path.c_str(), ofstream::out | ofstream::trunc);
    file.close();
}

int get_file_chars(string path) {
    ifstream file(path.c_str(), ifstream::binary);

    file.seekg(0,ios_base::end);
    ios_base::streampos end_pos = file.tellg();

    return end_pos;
}

bool test_key(string path, string key) {
    ifstream file(path.c_str(), ifstream::binary);
    string f_key;
    f_key.resize(key.size(), ' ');
    char* k_begin = &*f_key.begin();
    file.read(k_begin, key.size());
    f_key = decrypt(f_key, key);
    if (f_key.compare(key) == 0) return true;
    return false;
}

void generate_key(int ksize, string o_path) {
    string result = "";
    for (int i = 0; i < ksize; i++) {
        int c_i = randomNumberInt(0, chars.size() - 1);
        result += chars[c_i];
        getSetProgress((double)result.size() / (double)ksize);
    }

    ofstream file(o_path.c_str(), ofstream::out);
    file << result;
}

void encrypt_file(string path, string key, string output) {
    clear_file(output);

    ifstream file_in(path.c_str(), ifstream::binary);
    ofstream file_out(output.c_str(), ofstream::binary | ofstream::out | ofstream::app);

    char ch;

    int f_chars = get_file_chars(path);
    int p_chars = 0;

    file_out << encrypt(key, key) << endl;

    while (file_in >> noskipws >> ch) {
        string s_ch(1, ch);
        string en_char = encrypt(s_ch, key);
        file_out << en_char;
        ++p_chars;
        getSetProgress((double)p_chars / (double)f_chars);
    }
}

void decrypt_file(string path, string key, string output) {
    clear_file(output);

    ifstream file_in(path.c_str(), ifstream::binary);
    ofstream file_out(output.c_str(), ofstream::binary | ofstream::out | ofstream::app);

    char ch;

    int f_chars = get_file_chars(path);
    int p_chars = 0;

    bool r_key = test_key(path, key);
    if (!r_key) {
        cout << "{\"err\": \"Wrong password\"}" << endl;
        return;
    }
    file_in.seekg(key.size() + 1);
    f_chars -= key.size() + 1;
    while (file_in >> noskipws >> ch) {
        string s_ch(1, ch);
        string en_char = decrypt(s_ch, key);
        file_out << en_char;
        ++p_chars;
        getSetProgress((double)p_chars / (double)f_chars);
    }
}

/*
void help(int mode = 0) {
    cout << endl;
    if (mode == 0) {
        cout << "Encripta, decripta, gera chaves para arquivos\n"
        "\n"
        "-h: Mostra essa mensagem\n"
        "g_key [tamanho] [caminho de sa�da] *[mostra progresso=true/false]: Gera um arquivo de chave com um tamanho espec�fico\n"
        "encrypt [caminho do arquivo] ([chave]|[caminho para o arquivo de chave]) [caminho de sa�da] *[mostra progresso=true/false]: Encripta um arquivo\n"
        "decrypt [caminho do arquivo] ([chave]|[caminho para o arquivo de chave]) [caminho de sa�da] *[mostra progresso=true/false]: Decripta um arquivo\n"
        "*: opcional\n"
        "Aten��o: � aconselhado usar o programa sem mostrar o progresso, torna o processo muito mais r�pido" << endl;

    } else if (mode == 1) {
        cout << "g_key [tamanho] [caminho de sa�da]: Gera um arquivo de chave com um tamanho espec�fico\n" << endl;
    } else if (mode == 2) {
        cout << "encrypt [caminho do arquivo] ([chave]|[caminho para o arquivo de chave]) [caminho de sa�da]: Encripta um arquivo\n" << endl;
    } else if (mode == 3) {
        cout << "decrypt [caminho do arquivo] ([chave]|[caminho para o arquivo de chave]) [caminho de sa�da]: Decripta um arquivo\n" << endl;
    }
}*/

int main(int argc, char *argv[]) {
    clock_t t = clock();

    setlocale(LC_ALL, "");

    generate_ascii_codes();

    string mode(argv[1]);

    thread p(stdoutprogress);
    thread t1;

    if (mode.compare("g_key") == 0) {
        int ksize = stoi(argv[2]);
        string o_path(argv[3]);
        if (argc >= 5 && string(argv[4]).compare("false") == 0) {
            print_progress = false;
        }
        t1 = thread(generate_key, ksize, o_path.c_str());
    }
    if (mode.compare("encrypt") == 0) {
        string i_path(argv[2]);
        string key(argv[3]);
        string o_path(argv[4]);
        if (argc >= 6 && string(argv[5]).compare("false") == 0) {
            print_progress = false;
        }
        ifstream test_file_path(key.c_str());
        if (test_file_path) {
            test_file_path >> key;
        }
        t1 = thread(encrypt_file, i_path, key, o_path);
    }
    if (mode.compare("decrypt") == 0) {
        string i_path(argv[2]);
        string key(argv[3]);
        string o_path(argv[4]);
        if (argc >= 6 && string(argv[5]).compare("false") == 0) {
            print_progress = false;
        }
        ifstream test_file_path(key.c_str());
        if (test_file_path) {
            test_file_path >> key;
        }
        t1 = thread(decrypt_file, i_path, key, o_path);
    }

    t1.join();
    p.join();

    t = clock() - t;

    double delta = double(t) / CLOCKS_PER_SEC;

    cout << "{\"Delta\": " << fixed << setprecision(2) << delta << "}";

    return 0;
}
