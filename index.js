require('v8-compile-cache');

const {
    app,
    BrowserWindow,
    ipcMain,
} = require("electron")

const app_path = app.getAppPath();

const path = require("path")

const fs = require("fs")

const ENCT = require("encryptools")

global.sql = require("./js/sql.js");

async function load_exec(path, args = [], stdOut = () => {}, stdErr = () => {}, exit = () => {}) {
    let spawn = require('child_process').spawn;
    let exec = spawn(path, args);
    let readline = require('readline');

    readline.createInterface({
        input: exec.stdout,
        terminal: false
    }).on('line', stdOut);

    readline.createInterface({
        input: exec.stderr,
        terminal: false
    }).on('line', stdErr);

    exec.on('exit', exit);
}

global.themes = {}

global.app_path = app_path;
global.load_exec = load_exec;

global.config = {}

let appLoaded = 0
let appLoadParts = 0

let win;

async function loadConfig(loaded = () => {}) {
    fs.exists("config.json", (exists) => {
        if (exists) {
            fs.readFile("config.json", "utf8", (err, data) => {
                global.config = JSON.parse(data)
                config_changed(false)
                loaded()
            })
        } else {
            global.config = {
                "rootFolder": app.getPath('userData'),
                "databasePath": "",
                "encryptedFilesFolder": "",
                "themesFolder": "",
                "current_theme": "Light Theme",
            }
            config_changed()
            loaded()
        }
    })
}

function change_theme(new_theme) {
    global.config.current_theme = new_theme
    config_changed()
    win.webContents.send('reload-theme')
}

function scan_for_themes() {
    let themes_path = path.join(app_path, "themes")

    let themes = {}

    let dir = fs.readdirSync(themes_path)
    for (let dirpath of dir) {
        if (fs.existsSync(path.join(themes_path, dirpath, "info.json"))) {
            let theme_data = get_theme(path.join(themes_path, dirpath))

            themes[theme_data['name']] = theme_data
        }
    }

    global.themes = themes
}

function get_theme(dir_path) {
    let full_path = path.join(dir_path, "info.json")
    let json = fs.readFileSync(full_path, "utf8")

    let obj = JSON.parse(json)

    obj["stylesheet"] = path.join(dir_path, obj["stylesheet"])

    obj["demo_image"] = path.join(dir_path, obj["demo_image"])

    return obj
}

function config_changed(write = true) {
    global.config.databasePath = path.join(global.config.rootFolder, "database.dbenc")
    global.config.encryptedFilesFolder = path.join(global.config.rootFolder, "Encrypted Files")
    global.config.themesFolder = path.join(global.config.rootFolder, "themes")

    !fs.existsSync(global.config.encryptedFilesFolder) && fs.mkdirSync(global.config.encryptedFilesFolder)

    if (write) fs.writeFile("config.json", JSON.stringify(global.config, null, 2), () => {})
}

ipcMain.on('changed-config', () => {
    config_changed()
})

ipcMain.on('change-theme', (event, new_theme) => {
    change_theme(new_theme)
})

ipcMain.on('app-loading-part', (event, parts_num) => {
    appLoaded += parts_num
    appLoadParts += parts_num
    win.webContents.send('app-load-progress', appLoaded, appLoadParts)
})

ipcMain.on('app-loaded-part', () => {
    appLoaded -= 1
    win.webContents.send('app-load-progress', appLoaded, appLoadParts)
    if (appLoaded <= 0) {
        win.webContents.send('app-loadend')
    }
})

ipcMain.on('app-reloaded', () => {
    appLoaded = 0
    appLoadParts = 0
})

ipcMain.on('database-unlocked', () => {
    createWindow()
})

function createWindow() {
    if (!win) {
        win = new BrowserWindow({
            width: 1024,
            height: 600,
            minWidth: 512,
            minHeight: 300,
            webPreferences: {
                nodeIntegration: true,
                webviewTag: true,
            },
            frame: false,
        })
    } else {
        win.setSize(1024, 600)
        win.setMinimumSize(512, 300)
        win.center()
        win.resizable = true
    }

    win.loadFile('index.html')

    win.on('closed', () => {
        win = null
    })

    win.webContents.openDevTools()
}

function createAuthWin() {
    win = new BrowserWindow({
        width: 512,
        height: 300,
        resizable: false,
        webPreferences: {
            nodeIntegration: true,
            webviewTag: true,
        },
        transparent: true,
        frame: false,
    })

    win.loadFile('auth.html')

    win.on('closed', () => {
        win = null
    })

    win.webContents.openDevTools()
}

function createFirstUseWin() {
    win = new BrowserWindow({
        width: 700,
        height: 500,
        resizable: false,
        webPreferences: {
            nodeIntegration: true,
            webviewTag: true,
        },
        transparent: true,
        frame: false,
    })

    win.loadFile('first_use.html')

    win.on('closed', () => {
        win = null
    })

    win.webContents.openDevTools()
}

scan_for_themes()

app.on('ready', () => {
    loadConfig(() => {
        if (fs.existsSync(global.config.databasePath)) {
            createAuthWin()
        } else {
            createFirstUseWin()
        }
    })
})

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('browser-window-created', function (e, window) {
    window.setMenu(null)
})