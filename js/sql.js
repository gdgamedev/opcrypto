/*
	All the database logic will be here
*/

const encrypt = require("encryptools")

var key = ""
var database = ""
var data = {}
var open = false

function create_database(name = "", k = "") {
	database = name
	key = k
	data = {}
	open = true
	return encrypt.writeFile(name, '{}', k)
}

function open_database(name = "", k = "") {
	return encrypt.readFile(name, k).then((d) => {
		database = name
		key = k
		data = JSON.parse(d)

		open = true

		return d;
	}).catch((err) => {
		throw err
	})
}

function set(name = "", value) {
	data[name] = value
	return encrypt.writeFile(database, key, JSON.stringify(data))
}

function get(name = "") {
	if (name == "") {
		return data;
	}
	return data[name]
}

function remove(name = "") {
	delete data[name]
	return encrypt.writeFile(database, key, JSON.stringify(data))
}

module.exports = {
	create_database,
	open_database,
	set,
	get,
	remove,
	database,
	data,
	open
}