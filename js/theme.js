const {
	remote,
	ipcRenderer
} = require("electron")

let themeItemTemplate
let themeDescriptionTemplate

window.addEventListener('load', () => {
	//remote.getCurrentWebContents().openDevTools()
	themeItemTemplate = $(".theme-item")[0].cloneNode(true)
	themeDescriptionTemplate = $(".theme-description")[0].cloneNode(true)
	$(".theme-item").parent()[0].removeChild($(".theme-item")[0])
	$(".theme-description").parent()[0].removeChild($(".theme-description")[0])
	load_theme_itens()
})

function load_theme_itens() {
	let themes = remote.getGlobal("themes")
	let theme_list = $("#theme-list")[0]
	let theme_dec_list = $("#theme-descriptions")[0]
	for (let theme_data of Object.values(themes)) {
		let new_item = themeItemTemplate.cloneNode(true)

		$(new_item).find(".theme-name").html(theme_data["name"])

		let new_description = themeDescriptionTemplate.cloneNode(true)

		$(new_description).find(".theme-title").html(theme_data["name"])
		$(new_description).find(".theme-author").html("By " + theme_data["author"])
		$(new_description).find(".demo-image")[0].src = theme_data["demo_image"]

		if (theme_data["name"] == remote.getGlobal("config").current_theme) {
			$(new_item).addClass("opened")
			$(new_item).addClass("selected")
			$(new_description).addClass("show")
			$(new_description).addClass("selected")
			$(new_description).find(".theme-select").addClass("selected")
			$(new_description).find(".theme-select > .theme-select-text").html("Selected")
		}

		new_item.id = "item-" + theme_data["name"].split(" ").join("-")
		new_description.id = "desc-" + theme_data["name"].split(" ").join("-")

		theme_list.appendChild(new_item)
		theme_dec_list.appendChild(new_description)

		$(new_item).on('click', function () {
			let currentOpenTheme = $(".theme-description.show")[0]

			let itemThemeName = this.id.replace("item-", "")
			let itemOpened = $(".theme-item.opened")[0]

			if (this == itemOpened) return;

			$(currentOpenTheme).removeClass("show")
			$(itemOpened).removeClass("opened")

			let openNewTheme = $(".theme-description#desc-" + itemThemeName)

			openNewTheme.addClass("show")
			$(this).addClass("opened")
		})

		$(new_description).find(".theme-select").on('click', function () {
			if ($(this).hasClass("selected")) return;

			let themeName = $(this).parent()[0].id.replace("desc-", "")

			ipcRenderer.send("change-theme", themeName.split("-").join(" "))
		})
	}
}

ipcRenderer.on('reload-theme', () => {
	let current_theme = remote.getGlobal("config").current_theme
	current_theme = current_theme.split(" ").join("-")

	let current_item = $(".theme-item.selected")
	let current_desc = $(".theme-description.selected")
	let current_desc_select = current_desc.find(".theme-select")
	let current_desc_select_text = current_desc_select.find(".theme-select-text")

	current_item.removeClass("selected")
	current_desc.removeClass("selected")
	current_desc_select.removeClass("selected")
	current_desc_select_text.html("Select this theme")

	let new_item = $(".theme-item#item-" + current_theme)
	let new_desc = $(".theme-description#desc-" + current_theme)
	let new_desc_select = new_desc.find(".theme-select")
	let new_desc_select_text = new_desc_select.find(".theme-select-text")

	new_item.addClass("selected")
	new_desc.addClass("selected")
	new_desc_select.addClass("selected")
	new_desc_select_text.html("Selected")
})