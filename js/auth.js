const $ = require("jquery")

const {remote, ipcRenderer} = require("electron")

$("#cancel-btn").click(function() {
	remote.getCurrentWindow().close()
})

async function unlock() {
	$("#cancel-btn").prop("disabled", true)
	$("#unlock-btn").prop("disabled", true)
	let key = $("#unlock-password").val()
	if (!key) return;
	remote.getGlobal("sql").open_database(remote.getGlobal("config").databasePath, key)
	.then((d) => {
		ipcRenderer.send('database-unlocked')
	}).catch((err) => {
		$("#wrong-password").css("display", "flex")
		$("#cancel-btn").prop("disabled", false)
		$("#unlock-btn").prop("disabled", false)
	})
}

$("#unlock-btn").click(function() {
	unlock()
})