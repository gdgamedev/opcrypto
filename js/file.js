const {remote} = require("electron")
const path = require("path")
const encrypt = require("encryptools")

let activeEncryptDialogs = []
let fe

function openEncryptDialog(filePath, ev) {
	let passwordinput = $(`
	<div class="fe-fileencrypt">
        <span class="fe-encryptfilename">${path.basename(filePath)}</span>
        <div class="fe-fileencryptconfirmation">
            <span class="fe-confirmationtext">Are you sure to encrypt this file?</span>
            <div class="fe-confirmationoptions">
                <button class="fe-confirmation fe-encryptyes">Yes</button>
                <button class="fe-confirmation fe-encryptno">No</button>
            </div>
        </div>
	</div>
	`)
    
    activeEncryptDialogs.push(ev.currentTarget)

    let target = $(ev.currentTarget)

    let currentPosition = target.css("position")

    function close() {
        delete activeEncryptDialogs[activeEncryptDialogs.indexOf(ev.currentTarget)]
        target.css("position", currentPosition)
    }
	
    target.css("position", "relative")
    target.append(passwordinput)
    passwordinput.find('.fe-encryptno').click(() => {
        passwordinput.css("animation", "fileencrypthide forwards 1s ease-in-out")
        passwordinput.on('animationend', close)
    })
    passwordinput.find('.fe-encryptyes').click(() => {
        passwordinput.find('.fe-fileencryptconfirmation').css("opacity", "0")
        passwordinput.find('.fe-fileencryptconfirmation').on('transitionend', function() {
            let progressbar = $(`
                <div class="progress-bar">
                    <span class="progress-barpercent">0%</span> 
                </div>
            `)
            passwordinput.find('.fe-fileencryptconfirmation').replaceWith(progressbar)
            encrypt.encryptFile(filePath)
        })
    })
}

window.addEventListener('load', () => {
	const {FileExplorer, ContextMenu} = require("../libraries/gui-elements/script")

	let ctxmenu = new ContextMenu()
	fe = new FileExplorer(ctxmenu, require("os").homedir())

	fe.contextMenu = function(fileStats, filePath, ev) {
		if (fileStats.isFile()) {
            if (activeEncryptDialogs.includes(ev.currentTarget)) return;
            let menu = $(`
            <ctx-root>
                <ctx-option id="encrypt">Encrypt</ctx-option>
            </ctx-root>
            `)
            menu.find('#encrypt')[0].click = function () {
                openEncryptDialog(filePath, ev)
                return true;
            }
            fe.ctxmenu.open(menu, ev)
        } else if (fileStats.isDirectory()) {
            let menu = $(`
            <ctx-root>
                <ctx-option id="open">Open</ctx-option>
            </ctx-root>
            `)
            menu.find('#open')[0].click = function () {
                $(ev.currentTarget).dblclick()
                return true;
            }
            fe.ctxmenu.open(menu, ev)
        }
    }
    
    fe.jcode.on('updatedPath', function(ev) {
        activeEncryptDialogs = []
    })

	remote.getCurrentWebContents().openDevTools()
	$("#main").append(ctxmenu.jcode)
	$("#main").append(fe.jcode)
})