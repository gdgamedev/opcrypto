const $ = require("jquery")
window.jQuery = $;
require("jquery-ui-dist/jquery-ui")

const {
	remote,
	ipcRenderer,
} = require("electron");

const path = require("path");

const mouseTrap = require("mousetrap");

const fs = require("fs");

Math.lerp = function (a, b, n) {
	return (1 - n) * a + n * b;
}

let load_exec = remote.getGlobal("load_exec");
let app_path = remote.getGlobal("app_path");

let root = $(":root")
let current_theme = remote.getGlobal("config").current_theme
let tab_views = $(".tab-view")
let current_tab = $(".tab-view.current")

root.addClass(current_theme)

ipcRenderer.send('app-reloaded')

if (remote.getCurrentWindow().isMaximized()) {
	$("#max-app").addClass("hidden");
	$("#restore-app").removeClass("hidden");
} else {
	$("#restore-app").addClass("hidden");
	$("#max-app").removeClass("hidden");
}

$('#minimize-app').click(function () {
	remote.getCurrentWindow().minimize()
})

$("#max-app").click(function () {
	remote.getCurrentWindow().maximize();
	$("#max-app").addClass("hidden");
	$("#restore-app").removeClass("hidden");
})

$("#restore-app").click(function () {
	remote.getCurrentWindow().unmaximize();
	$("#restore-app").addClass("hidden");
	$("#max-app").removeClass("hidden");
})

$('#close-app').click(function () {
	remote.getCurrentWindow().close()
})

let tabs = $("div.tab");
tabs.each(function (id, el) {
	let tab_name = $(el).attr("id")
	$(el).click(function () {
		let active = $(".tab.active");
		if (active == el) return;
		active.removeClass("active")
		$(el).addClass("active");
		changeTab("view-" + tab_name)
	});
})

let dropdownButtons = $(".dropdown-button")

dropdownButtons.click(function () {
	let content = $(this).find("+.dropdown-content")
	content.toggleClass("show")
})

function clickEvent(event) {
	if (!event.target.matches('.dropdown-button')) {
		let dropdown_contents = $(".dropdown-content")
		dropdown_contents.each(function () {
			$(this).removeClass("show")
		})
	}
}

window.onclick = clickEvent
tab_views.each(function () {
	this.addEventListener('ipc-message', function (event) {
		clickEvent(event)
	})
})

function screenShot(file_path) {
	const webContents = remote.getCurrentWebContents()
	const FilePath = path.join(remote.app.getAppPath(), file_path)

	webContents.capturePage().then(function (img) {
		remote.require('fs').writeFile(FilePath, img.toPNG(), (err) => {
			if (err) throw err;

			console.log('File saved!');
		});
	});
}

mouseTrap.bind("ctrl+p", function () {
	let date = new Date()

	let year = date.getFullYear()
	let month = date.getMonth() + 1
	let day = date.getDate()

	let hour = date.getHours()
	let minute = date.getMinutes()
	let seconds = date.getSeconds()

	let file_name = `screenshot_${year}_${month}_${day}_${hour}_${minute}_${seconds}.png`

	screenShot(file_name)
})

let themes = remote.getGlobal("themes")

function load_themes() {
	ipcRenderer.send('app-loading-part', Object.keys(themes).length)

	let current_theme = remote.getGlobal("config").current_theme

	for (let theme_data of Object.values(themes)) {
		let stylesheet_path = theme_data["stylesheet"]

		let head = document.head
		let link = document.createElement("link")

		link.classList.add("theme-style")
		link.id = theme_data["name"].split(" ").join("-")
		link.rel = "stylesheet"
		link.type = "text/css"
		link.href = stylesheet_path

		link.onload = function () {
			if (this.id != current_theme.split(" ").join("-")) {
				this.disabled = true
			}
			ipcRenderer.send("app-loaded-part", "style")
		}

		head.appendChild(link)
	}
}

function reload_theme() {
	$(".theme-style").each(function () {
		this.disabled = true
	})
	let current_theme = remote.getGlobal("config").current_theme
	current_theme = "#" + current_theme.split(" ").join("-")
	$(current_theme)[0].disabled = false
}

function changeTab(new_tab) {
	current_tab.removeClass("current")

	current_tab = $(".tab-view#" + new_tab)

	current_tab.addClass("current")
}

load_themes()

tab_views.each(function () {
	if (!$(this).hasClass("current")) $(this).addClass("current")

	if (this.getAttribute('src')) {
		ipcRenderer.send('app-loading-part', Object.keys(themes).length + 1)
	}
})

$("#sidebar").resizable({
	handles: 'e',
	minWidth: parseInt($("#sidebar").css("min-width"), 10),
	maxWidth: parseInt($("#sidebar").css("max-width"), 10),
	resize: function(event, ui) {
		let minWidth = parseInt($("#sidebar").css("min-width"), 10)
		let maxWidth = parseInt($("#sidebar").css("max-width"), 10)
		let size_percent = (ui.size.width - minWidth) / (maxWidth - minWidth)
		let font = Math.lerp(minWidth * 0.5, maxWidth * 0.5, size_percent)
		$(".tab-icon").css("font-size", font)
	},
	start: function(event, ui) {
		$(".tab-view").css("pointer-events", "none")
	},
	stop: function(event, ui) {
		$(".tab-view").css("pointer-events", "initial")
	}
})

ipcRenderer.on('reload-theme', () => {
	reload_theme()
	tab_views.each(function () {
		try {
			this.send('reload-theme')
		} catch {}
	})
})

ipcRenderer.on('app-load-progress', (event, loaded, total) => {
	let progress = (total - loaded) / total
	$("#loading-bar").css("width", (progress * 100) + "%")
})

ipcRenderer.on('app-loadend', () => {
	reload_theme()
	tab_views.each(function () {
		try {
			this.send('reload-theme')
		} catch {}
	})
	setTimeout(() => {
		$(".tab-view.current").removeClass("current")
		changeTab(current_tab.attr("id"))
	}, 100)
	setTimeout(() => {
		$("#loading-screen").css("opacity", "0")
		$("#loading-screen")[0].addEventListener("transitionend", () => {
			$("#loading-screen").css("display", "none")
		})
	}, 2000)
})