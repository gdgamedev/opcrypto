const $ = require("jquery")

const {remote, ipcRenderer} = require("electron")

let tab = 0;

function timeout(callback = () => {}, time_msc = 0, update = (elapsed, remain) => {}) {
	let elapsed = 0;
	let last = Date.now();
	let process = setInterval(() => {
		let now = Date.now();
		let delta = now - last;
		last = Date.now();

		elapsed += delta;

		elapsed = Math.min(elapsed, time_msc)

		update(elapsed, time_msc - elapsed)

		if (elapsed >= time_msc) {
			callback();
			clearInterval(process)
		}
	}, 0)
}

$(".next").click(function () {
	tab += 1

	if ($(".slide#slide" + tab).length == 0) {
		ipcRenderer.send("database-unlocked")
		return;
	}

	let width = $("#slides").css("width").replace("px", "")

	$("#slides").animate({
		scrollLeft: Number(width) * tab
	}, {
		duration: 'medium',
		easing: 'swing'
	})

	let next = $(".slide#slide" + tab)
	let next_btn = next.find(".next")
	let disabled_time = next_btn.attr("disabled-time") * 1000
	if (disabled_time) {
		function updateTimer(elapsed, remain) {
			remain /= 1000
			next_btn.find(".timeout").html(remain.toFixed(0) + "s")
		}
		timeout(function () {
			next_btn.prop("disabled", false)
			next_btn.find(".timeout").css("display", "none")
		}, disabled_time, updateTimer)
	}
})

export function data_path() {
	let choice = $("#slide2>form").serializeArray()[0].value;
	if (choice == "Local") {
		remote.getGlobal("config").rootFolder = remote.app.getAppPath()
		ipcRenderer.send("changed-config")
	}
	if (choice == "UserData") {
		remote.getGlobal("config").rootFolder = remote.app.getPath('userData')
		ipcRenderer.send("changed-config")
	}
}

export function password() {
	let pass = $("#slide3>form").serializeArray()[0].value;
	remote.getGlobal("sql").create_database(remote.getGlobal("config").databasePath, pass)
	$("#slide3>form")[0].reset()
}