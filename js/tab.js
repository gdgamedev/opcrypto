const { ipcRenderer, remote } = require('electron');

window.addEventListener('load', () => {
	window.$ = window.jQuery = require('jquery')
	require("jquery-ui-dist/jquery-ui")

	//remote.getCurrentWebContents().openDevTools()

	let root = $(":root")
	let current_theme = remote.getGlobal("config").current_theme

	root.addClass(current_theme)

	load_themes()

	ipcRenderer.send('app-loaded-part', "loaded tab")
})

window.addEventListener('click', function(event) {
	ipcRenderer.sendToHost("window-click", event)
})

function load_themes() {
	let themes = remote.getGlobal("themes")

	let current_theme = remote.getGlobal("config").current_theme

	for (let theme_data of Object.values(themes)) {
		let stylesheet_path = theme_data["stylesheet"]

		let head = document.head
		let link = document.createElement("link")

		link.classList.add("theme-style")
		link.id = theme_data["name"].split(" ").join("-")
		link.rel = "stylesheet"
		link.type = "text/css"
		link.href = stylesheet_path

		link.onload = function() {
			if (this.id != current_theme.split(" ").join("-")) {
				this.disabled = true
			}
			ipcRenderer.send("app-loaded-part", "style")
		}

		head.appendChild(link)
	}
}

function reload_theme() {
	$(".theme-style").each(function() {
		this.disabled = true
	})
	let current_theme = remote.getGlobal("config").current_theme
	current_theme = "#"+current_theme.split(" ").join("-")
	$(current_theme)[0].disabled = false
}

ipcRenderer.on('reload-theme', () => {
	reload_theme()
})